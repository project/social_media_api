<?php

/**
 * Facebook Graph API configuration form.
 */
function social_media_api_configure_form($form, &$form_state) {
  $secure_connection = $_SERVER["SERVER_PORT"] == 443 ? TRUE : FALSE;
  $facebook_auto = isset($form_state['values']['social_media_api_facebook_auto']) ? $form_state['values']['social_media_api_facebook_auto'] : variable_get('social_media_api_facebook_auto', 0);

  $facebook_id = variable_get('social_media_api_facebook_app_id');
  $facebook_secret = variable_get('social_media_api_facebook_app_secret');
  $form['facebook'] = [
    '#type' => 'fieldset',
    '#title' => t('Facebook and Instagram'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];

  if ($secure_connection) {
    $form['facebook']['social_media_api_facebook_auto'] = [
      '#type' => 'checkbox',
      '#title' => t('Automatic configuration.'),
      '#default_value' => variable_get('social_media_api_facebook_auto', 0),
      '#ajax' => [
        'callback' => 'social_media_api_facebook_auto_ajax_callback',
        'wrapper' => 'edit-facebook-info',
        'method' => 'html',
        'effect' => 'fade',
      ],
    ];
  }
  else {
    $facebook_auto = 0;
    variable_set('social_media_api_facebook_auto', 0);
  }

  $form['facebook']['social_media_api_facebook_app_id'] = [
    '#type' => 'textfield',
    '#default_value' => variable_get('social_media_api_facebook_app_id', ''),
    '#title' => t('App ID'),
    '#required' => TRUE,
  ];

  $form['facebook']['social_media_api_facebook_app_secret'] = [
    '#type' => 'password',
    '#default_value' => '',
    '#title' => t('App Secret'),
    '#description' => variable_get('social_media_api_facebook_app_secret', '') ? t('Leave blank to keep previous value.') : '',
  ];

  if (variable_get('social_media_api_facebook_app_secret', '') == '') {
    $form['facebook']['social_media_api_facebook_app_secret']['#required'] = TRUE;
  }
  $form['facebook']['facebook_info'] = [
    '#type' => 'container',
  ];

  if ($facebook_auto) {

    if ($facebook_id && $facebook_secret) {
      $socialMediaApiFacebook = new SocialMediaApiFacebook();

      if (!variable_get('social_media_api_facebook_app_access_token')) {
        $login_url = $socialMediaApiFacebook->getLoginUrl(
          url('admin/config/services/social-media-api/fb-auth', ['absolute' => TRUE]),
          ['instagram_basic', 'pages_show_list']
        );

        $form['facebook']['facebook_info']['facebook_authenticate'] = [
          '#type' => 'link',
          '#title' => t('Authenticate your Facebook account'),
          '#href' => $login_url,
        ];
      } else {
        $facebookPages = ['' => 'Select Facebook page'] + $socialMediaApiFacebook->getUserPages();

        $facebookPageId = isset($form_state['values']['social_media_api_facebook_page']) ? $form_state['values']['social_media_api_facebook_page'] : variable_get('social_media_api_facebook_page', NULL);
        $form['facebook']['facebook_info']['social_media_api_facebook_page'] = [
          '#type' => 'select',
          '#title' => t('Facebook page'),
          '#options' => $facebookPages,
          '#default_value' => $facebookPageId,
          '#ajax' => array(
            'callback' => 'social_media_api_facebook_page_ajax_callback',
            'wrapper' => 'edit-facebook-info',
            'method' => 'html',
            'effect' => 'fade',
          ),
        ];

        if ($facebookPageId) {
          $instagram_account = $socialMediaApiFacebook->getPageInstagramBusinessAccount($facebookPageId);
          $instagramAccountId = isset($form_state['values']['social_media_api_facebook_instagram_account']) ? $form_state['values']['social_media_api_facebook_instagram_account'] : variable_get('social_media_api_facebook_instagram_account', NULL);
          $form['facebook']['facebook_info']['social_media_api_facebook_instagram_account'] = [
            '#type' => 'select',
            '#title' => t('Instagram Account ID'),
            '#options' => ['' => 'Select Instagram Account ID'] + $instagram_account,
            '#default_value' => $instagramAccountId,
          ];
        }

        $form['facebook']['facebook_info']['update_token'] = [
          '#type' => 'submit',
          '#value' => t('Update access token'),
          '#submit' => ['social_media_api_configure_form_facebook_update_token_submit'],
        ];

        $form['facebook']['facebook_info']['logout'] = [
          '#type' => 'submit',
          '#value' => t('logout'),
          '#submit' => ['social_media_api_configure_form_facebook_logout_submit'],
        ];

      }
    }
  }
  else {
    $form['facebook']['facebook_info']['social_media_api_facebook_app_access_token'] = [
      '#type' => 'textfield',
      '#default_value' => variable_get('social_media_api_facebook_app_access_token', ''),
      '#title' => t('Access Token'),
      '#maxlength' => 300,
      '#required' => TRUE,
    ];

    $form['facebook']['facebook_info']['social_media_api_facebook_page'] = [
      '#type' => 'textfield',
      '#default_value' => variable_get('social_media_api_facebook_page', ''),
      '#title' => t('Facebook page ID'),
      '#maxlength' => 300,
      '#required' => TRUE,
    ];

    $form['facebook']['facebook_info']['social_media_api_facebook_instagram_account'] = [
      '#type' => 'textfield',
      '#default_value' => variable_get('social_media_api_facebook_instagram_account', ''),
      '#title' => t('Instagram Account ID'),
      '#maxlength' => 300,
      '#required' => TRUE,
    ];
  }

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Save configuration',
  ];
  return $form;
}

function social_media_api_facebook_auto_ajax_callback($form, &$form_state) {
  $facebook_auto = $form_state['values']['social_media_api_facebook_auto'];
  variable_set('social_media_api_facebook_auto', $facebook_auto);

  $form_state['rebuild'] = TRUE;
  return $form['facebook']['facebook_info'];
}

function social_media_api_facebook_page_ajax_callback($form, &$form_state) {
  variable_set('social_media_api_facebook_page', $form_state['values']['social_media_api_facebook_page']);
  return $form['facebook']['facebook_info'];
}

function social_media_api_configure_form_facebook_update_token_submit($form, &$form_state) {
  $facebook_id = variable_get('social_media_api_facebook_app_id');
  $facebook_secret = variable_get('social_media_api_facebook_app_secret');
  if ($facebook_id && $facebook_secret) {
    $socialMediaApiFacebook = new SocialMediaApiFacebook();
    variable_set('social_media_api_facebook_app_access_token', '');
    $login_url = $socialMediaApiFacebook->getLoginUrl(
      url('admin/config/services/social-media-api/fb-auth', ['absolute' => TRUE]),
      ['instagram_basic', 'pages_show_list']
    );
    drupal_goto($login_url);
  }
  else {
    drupal_set_message('Cannot update access token', 'error');
  }

}

function social_media_api_configure_form_facebook_logout_submit($form, &$form_state) {
  variable_set('social_media_api_facebook_app_access_token', '');
  variable_set('social_media_api_facebook_app_secret', '');
  variable_set('social_media_api_facebook_page', '');
  variable_set('social_media_api_facebook_instagram_account', '');
  drupal_set_message('You have been successfully logged out.');

}

function social_media_api_configure_form_submit($form, &$form_state) {

  if (isset($form_state['values']['social_media_api_facebook_auto'])) {
    variable_set('social_media_api_facebook_auto', $form_state['values']['social_media_api_facebook_auto']);
  }
  variable_set('social_media_api_facebook_app_id',  $form_state['values']['social_media_api_facebook_app_id']);
  if ($form_state['values']['social_media_api_facebook_app_secret'] != '') {
    variable_set('social_media_api_facebook_app_secret', $form_state['values']['social_media_api_facebook_app_secret']);
  }
  if (isset($form_state['values']['social_media_api_facebook_app_access_token'])) {
    variable_set('social_media_api_facebook_app_access_token', $form_state['values']['social_media_api_facebook_app_access_token']);
  }
  if (isset($form_state['values']['social_media_api_facebook_page'])) {
    variable_set('social_media_api_facebook_page', $form_state['values']['social_media_api_facebook_page']);
  }
  if (isset($form_state['values']['social_media_api_facebook_instagram_account'])) {
    variable_set('social_media_api_facebook_instagram_account', $form_state['values']['social_media_api_facebook_instagram_account']);
  }

  drupal_set_message('The configuration options have been saved.');
}

function social_media_api_facebook_oauth_page() {
  $socialMediaApiFacebook = new SocialMediaApiFacebook();
  $fb = $socialMediaApiFacebook->getFacebook();

  if ($fb) {
    $login = $fb->getRedirectLoginHelper();
    $params = drupal_get_query_parameters();
    $state = isset($params['state']) ? $params['state'] : FALSE;
    $login->getPersistentDataHandler()->set('state', $state);

    try {
      $accessToken = $login->getAccessToken();
      if ($accessToken) {
        if (!$accessToken->isLongLived()) {
          $accessToken = $fb->getOAuth2Client()->getLongLivedAccessToken($accessToken);
        }
        $accessToken = $accessToken->getValue();
        variable_set('social_media_api_facebook_app_access_token', $accessToken);
        drupal_set_message(t('Your account is successfully linked to your website.'));
      }
    }
    catch (\Exception $e) {
      drupal_set_message(t('We were unable to retrieve the long-lived access token.'), 'error');
      watchdog_exception('social_media_api', $e);
    }
  }

  drupal_goto('admin/config/services/social-media-api');
}
