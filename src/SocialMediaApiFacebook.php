<?php

use Facebook\Facebook;

/**
 * Class SocialMediaApiFacebook.
 */
class SocialMediaApiFacebook {
  /**
   * Facebook SDK library instance.
   *
   * @var \Facebook\Facebook
   */
  protected $facebook;

  public function __construct() {
    // Attempt manual autoloader if needed.
    if (!class_exists('\Facebook\Facebook') && file_exists(SOCIAL_MEDIA_API_FACEBOOK_AUTOLOADER)) {
      require_once SOCIAL_MEDIA_API_FACEBOOK_AUTOLOADER;
    }
    $this->getFacebook();
  }

  public function getFacebook() {
    $id = variable_get('social_media_api_facebook_app_id');
    $secret = variable_get('social_media_api_facebook_app_secret');

    if ($id != '' && $secret != '') {
      $this->facebook = new Facebook([
        'app_id' => $id,
        'app_secret' => $secret,
        'default_graph_version' => SOCIAL_MEDIA_API_FACEBOOK_GRAPH_VERSION,
      ]);

      return $this->facebook;
    }
  }

  public function getLoginUrl($redirectUrl, array $permissions) {
    $url = '';

    if ($this->facebook) {
      $url = $this->facebook->getRedirectLoginHelper()->getLoginUrl($redirectUrl, $permissions);
    }
    return $url;
  }

  public function getUserPages($userId = 'me') {
    $pages = [];
    if ($this->facebook) {

      try {
        $response = $this->facebook->get('/' . $userId . '/accounts?summary=total_count&limit=35', variable_get('social_media_api_facebook_app_access_token'));
        $accountsGraphNodes = $response->getGraphEdge();

        $accounts = [];

        do {
          $accounts = array_merge($accounts, $accountsGraphNodes->asArray());
        } while ($accountsGraphNodes = $this->facebook->next($accountsGraphNodes));

        foreach ($accounts as $page) {
          $pages[$page['id']] = $page['name'];
        }

      }
      catch (Exception $e) {
        watchdog('social_media_api', $e->getMessage(), array(), WATCHDOG_ERROR);
      }
    }
    return $pages;
  }

  public function getPageInstagramBusinessAccount($pageId) {

    $instagramBusinessAccount = [];
    if ($this->facebook) {
      try {
        $response = $this->facebook->get('/' . $pageId . '?fields=instagram_business_account', variable_get('social_media_api_facebook_app_access_token'));
        $decodedBody = $response->getDecodedBody();
        if (isset($decodedBody['instagram_business_account'])) {
          $instagramBusinessAccountId = $decodedBody['instagram_business_account']['id'];
          $response = $this->facebook->get('/' . $instagramBusinessAccountId . '?fields=username,name', variable_get('social_media_api_facebook_app_access_token'));
          $decodedBody = $response->getDecodedBody();
          $instagramBusinessAccount[$instagramBusinessAccountId] = $decodedBody['name'] . ' (@' . $decodedBody['username'] . ')';
        }
      }
      catch (Exception $e) {
        watchdog('social_media_api', $e->getMessage(), array(), WATCHDOG_ERROR);
      }
    }
    return $instagramBusinessAccount;
  }

  public function getFacebookPosts($limit = 10, $cache = TRUE, $cache_id = '', $expire = CACHE_PERMANENT, $data = []) {
    $cache_id = 'social_media_api_facebook_posts_' . $cache_id;
    if ($cached = cache_get($cache_id, 'cache'))  {
      $data = $cached->data;
      if (!empty($data)) {
        return $data;
      }
    }
    if (!isset($data['fields'])) {
      $data['fields'] = ['id', 'message', 'created_time', 'permalink_url'];
    }
    $facebookPageId = variable_get('social_media_api_facebook_page');
    if ($this->facebook) {
      try {
        $endpoint = url(
          '/' . $facebookPageId . '/posts',
          [
            'query' => [
              'fields' => implode(",",  $data['fields']),
              'limit' => $limit
            ],
            'external' => TRUE
          ]
        );
        $response = $this->facebook->get($endpoint, variable_get('social_media_api_facebook_app_access_token'));
        if (empty($response)) {
          return [];
        }
        $posts = json_decode($response->getBody())->data;
        if (!$response->isError() && !empty($posts)) {
          cache_set($cache_id, $posts, 'cache', $expire);
          return $posts;
        }

      }
      catch (Exception $e) {
        watchdog('social_media_api', $e->getMessage(), array(), WATCHDOG_ERROR);
      }

    }
  }

  public function getInstagramPosts($limit = 10, $cache = TRUE, $cache_id = '', $expire = CACHE_PERMANENT, $data = []) {
    $cache_id = 'social_media_api_instagram_posts_' . $cache_id;
    if ($cached = cache_get($cache_id, 'cache'))  {
      $data = $cached->data;
      if (!empty($data)) {
        return $data;
      }
    }
    if (!isset($data['fields'])) {
      $data['fields'] = ['timestamp', 'caption', 'like_count', 'media_type', 'media_url', 'permalink'];
    }
    $posts = [];
    $instagramBusinessAccountId = variable_get('social_media_api_facebook_instagram_account');
    if ($this->facebook) {
      try {
        $endpoint = url(
          '/' . $instagramBusinessAccountId . '/media',
          [
            'query' => [
              'fields' => implode(",",  $data['fields']),
              'limit' => $limit
            ],
            'external' => TRUE
          ]
        );
        $response = $this->facebook->get($endpoint, variable_get('social_media_api_facebook_app_access_token'));
        $mediaGraphNodes = $response->getGraphEdge();
        $media = [];
        do {
          $filteredMedia = array_filter(
            $mediaGraphNodes->asArray(),
            function ($media) {
              return $media['media_type'] == 'IMAGE';
            }
          );
          $media = array_merge($media, $filteredMedia);
          if (count($media) >= $limit) {
            break;
          }
        } while ($mediaGraphNodes = $this->facebook->next($mediaGraphNodes));

        foreach ($media as $image) {
          $posts[$image['id']] = $image;
        }

      }
      catch (Exception $e) {
        watchdog('social_media_api', $e->getMessage(), array(), WATCHDOG_ERROR);
      }
    }
    cache_set($cache_id, $posts, 'cache', $expire);
    return $posts;
  }

}
